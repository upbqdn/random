//
// Created by marek on 3/3/18.
//

#ifndef PDP_CONSTANTS_HPP
#define PDP_CONSTANTS_HPP

#define SOL_BYTE_SIZE 3

static const std::string WRONG_ARGS{"Wrong args."};
static const std::string NO_FILE{"No such file."};

enum Args {
    MPI_BFS_DEPTH = 1,
    OMP_BFS_DEPTH,
    TASK_DEPTH,
    THREADS_NUM,
    FILE_PATH,
    ARGS_END
};

#endif //PDP_CONSTANTS_HPP
