//
// Created by marek on 3/4/18.
//

#ifndef PDP_FIELD_HPP
#define PDP_FIELD_HPP

#include <vector>
#include <queue>
#include <set>
#include <sstream>
#include <omp.h>
#include "helpers.hpp"

struct Params;

enum Directions {
    up,
    leftUp,
    left,
    leftDown,
    down,
    rightDown,
    right,
    upRight,
    last
};

typedef std::pair<uint16_t, uint16_t> Position;

typedef std::pair<Position, bool> Move;

typedef std::vector<Move> Solution;

class State {
public:
    State(Position &t_pos, State &t_prev_state, bool t_contains_piece);

    State(Params t_params);

    State();

    template<class T>
    void explore(T &t_states);

    static Solution const getSolution();

    unsigned getDepth() const;

    bool containsPiece() const;

protected:
    template<class T>
    void expand(T &t_states);

private:
    //global members for mpi, no need to transfer
    static uint16_t m_board_size;
    static uint16_t m_init_pieces_num;
    static Solution m_best_sol;
    static std::set<Position> m_white_pieces;

    //local members, will be transferred
    static uint16_t m_best_depth;
    bool m_contains_piece{false};
    Position m_pos;
    Solution m_sol;
    uint16_t m_depth{0};
    uint16_t m_taken_pieces_num{0};
    std::set<Position> m_rem_pieces;
};

struct BFSCmpState {
    bool operator()(State const &lhs, State const &rhs);
};

struct DFSCmpState {
    bool operator()(State const &lhs, State const &rhs);
};

struct CmpState {
    bool operator()(State const &lhs, State const &rhs);
};

#include "State.tpp"

#endif //PDP_FIELD_HPP