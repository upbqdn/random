//
// Created by marek on 4/9/18.
//

template<class T>
void State::explore(T &t_states) {
    using namespace std;

    if (m_contains_piece) {
        m_sol.push_back(make_pair(m_pos, true));
        m_rem_pieces.erase(m_pos);
        ++m_taken_pieces_num;


        if (m_init_pieces_num == m_taken_pieces_num && m_depth < m_best_depth) {
#pragma omp critical
            {
                if (m_init_pieces_num == m_taken_pieces_num &&
                    m_depth < m_best_depth) {
                    m_best_depth = m_depth;
                    m_best_sol = m_sol;
                }
            }

            return;
        }
    } else {
        m_sol.push_back(make_pair(m_pos, false));
    }

    if (m_depth + (m_init_pieces_num - m_taken_pieces_num) >= m_best_depth) {
        return;
    }

    expand(t_states);
}

template<class T>
void State::expand(T &t_states) {
    using namespace std;

    for (int direction = up; direction != last; ++direction) {
        Position next_pos = m_pos;

        while (next_pos.first < m_board_size &&
               next_pos.second < m_board_size) {
            switch (direction) {
                case up:
                    ++next_pos.second;
                    break;
                case leftUp:
                    ++next_pos.second;
                    --next_pos.first;
                    break;
                case Directions::left:
                    --next_pos.first;
                    break;
                case leftDown:
                    --next_pos.first;
                    --next_pos.second;
                    break;
                case down:
                    --next_pos.second;
                    break;
                case rightDown:
                    ++next_pos.first;
                    --next_pos.second;
                    break;
                case Directions::right:
                    ++next_pos.first;
                    break;
                case upRight:
                    ++next_pos.second;
                    ++next_pos.first;
                    break;
                default:
                    break;
            }

            if (next_pos.first >= m_board_size ||
                next_pos.second >= m_board_size ||
                m_white_pieces.end() != m_white_pieces.find(next_pos)) {
                break;
            } else if (m_rem_pieces.end() != m_rem_pieces.find(next_pos)) {
                t_states.push(State(next_pos, *this, true));
                break;
            }

            t_states.push(State(next_pos, *this, false));
        }
    }
}