//
// Created by marek on 3/4/18.
//

#ifndef PDP_HELPERS_HPP
#define PDP_HELPERS_HPP

#include <fstream>
#include <iostream>
#include <chrono>
#include "constants.hpp"
#include "State.hpp"

typedef std::pair<uint16_t, uint16_t> Position;

typedef std::pair<Position, bool> Move;

typedef std::vector<Move> Solution;

struct Params {
    unsigned board_size;
    unsigned init_best_depth;
    std::set<Position> white_pieces;
    std::set<Position> rem_pieces;
    Position pos;
    unsigned mpi_bfs_depth;
    unsigned omp_bfs_depth;
    unsigned threads_num;
    unsigned task_depth;
    std::string file_name;

    void parse(int t_argc, char *t_argv[]);
};

void getFile(std::string t_file_name, /*out*/ std::ifstream &t_file);

void printResults(Solution t_sol, std::chrono::duration<double> t_duration,
                  Params t_params);

std::string durationToString(std::chrono::duration<double> t_duration);

#endif //PDP_HELPERS_HPP
