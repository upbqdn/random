//
// Created by marek on 3/6/18.
//

#ifndef PDP_SOLVER_HPP
#define PDP_SOLVER_HPP

#include "State.hpp"

class Solver {
public:
    Solver(Params t_params);

    Solution solve(State &t_init_state, int r, int p);

    std::chrono::duration<double> getDuration() const;

protected:
    void recursiveDFSSolve(State &t_state);

    void iterativeDFSSolve(State &t_state);

    void localSolve(State &t_state);

private:
    std::chrono::duration<double> m_duration;
    unsigned m_max_task_depth;
    unsigned m_mpi_bfs_depth;
    unsigned m_omp_bfs_depth;
    unsigned m_threads_num;
};


#endif //PDP_SOLVER_HPP
