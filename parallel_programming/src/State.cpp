//
// Created by marek on 3/4/18.
//

#include "State.hpp"

Solution State::m_best_sol;
uint16_t State::m_board_size;
uint16_t State::m_init_pieces_num;
uint16_t State::m_best_depth;
std::set<Position> State::m_white_pieces;

State::State(Position &t_pos, State &t_prev_state, bool t_contains_piece) :
        m_contains_piece(t_contains_piece),
        m_pos(t_pos),
        m_sol(t_prev_state.m_sol),
        m_depth(t_prev_state.m_depth + 1),
        m_taken_pieces_num(t_prev_state.m_taken_pieces_num),
        m_rem_pieces(t_prev_state.m_rem_pieces) {
}

State::State(Params t_params) :
        m_pos(t_params.pos),
        m_rem_pieces(t_params.rem_pieces) {
    m_board_size = t_params.board_size;
    m_best_depth = t_params.init_best_depth;
    m_white_pieces = t_params.white_pieces;
    m_init_pieces_num = t_params.rem_pieces.size();
}

Solution const State::getSolution() {
    return m_best_sol;
}

unsigned State::getDepth() const {
    return m_depth;
}

bool State::containsPiece() const {
    return m_contains_piece;
}

State::State() {
}

bool BFSCmpState::operator()(State const &lhs, State const &rhs) {
    if (lhs.getDepth() > rhs.getDepth()) {
        return true;
    } else if (lhs.getDepth() == rhs.getDepth()) {
        return rhs.containsPiece();
    }

    return false;
}

bool DFSCmpState::operator()(State const &lhs, State const &rhs) {
    if (lhs.getDepth() < rhs.getDepth()) {
        return true;
    } else if (lhs.getDepth() == rhs.getDepth()) {
        return rhs.containsPiece();
    }

    return false;
}

bool CmpState::operator()(State const &lhs, State const &rhs) {
    return rhs.containsPiece();
}

