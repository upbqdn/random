#include "main.hpp"
#include <iostream>
#include <vector>
#include <mpi.h>
#include "State.hpp"
#include "Solver.hpp"

int main(int argc, char *argv[]) {
    using namespace std;
    using namespace std::chrono;

    int r = 0;
    int p = 1;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &r);
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    Params params;

    try {
        params.parse(argc, argv);
    } catch (const string &msg) {
        cerr << msg << endl;
        return EXIT_FAILURE;
    }

    State init_state(params);
    Solver solver(params);
    const auto start = high_resolution_clock::now();
    Solution sol = solver.solve(init_state, r, p);

    if (r) {
        MPI_Send(sol.data(), sol.size() * sizeof(Move), MPI_BYTE, 0, 0,
                 MPI_COMM_WORLD);
    } else {
        for (int i = 1; i < p; ++i) {
            MPI_Status s;
            int c = 0;
            MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &s);
            MPI_Get_count(&s, MPI_CHAR, &c);
            auto *b = (Move *) malloc(c);
            MPI_Recv(b, c, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG,
                     MPI_COMM_WORLD, &s);
            if (c / sizeof(Move) < sol.size())
                sol = Solution(b, b + c / sizeof(Move));
            free(b);
        }

        printResults(sol, high_resolution_clock::now() - start, params);
    }

    MPI_Finalize();

    return EXIT_SUCCESS;
}