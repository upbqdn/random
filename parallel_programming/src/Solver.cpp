//
// Created by marek on 3/6/18.
//

#include "Solver.hpp"

Solver::Solver(Params t_params) :
        m_max_task_depth(t_params.task_depth),
        m_mpi_bfs_depth(t_params.mpi_bfs_depth),
        m_omp_bfs_depth(t_params.omp_bfs_depth),
        m_threads_num(t_params.threads_num) {
}

Solution
Solver::solve(State &t_s, int r, int p) {
    using namespace std;

    priority_queue<State, vector<State>, BFSCmpState> q;
    q.push(t_s);

    while (q.size() < m_mpi_bfs_depth) {
        State curr_s = q.top();
        q.pop();
        curr_s.explore(q);
    }

    //set the offset for each rank (node)
    for (int i = 0; i < r; ++i) {
        q.pop();
    }

    while (!q.empty()) {
        State s = q.top();
        localSolve(s);

        //shift the position in the queue according to the num of nodes
        for (int i = 0; i < p; ++i) {
            if (!q.empty())
                q.pop();
        }
    }

    return State::getSolution();
}

void
Solver::localSolve(State &t_state) {
    using namespace std;
    using namespace std::chrono;

    const auto start = high_resolution_clock::now();
    if (m_omp_bfs_depth) {
        priority_queue<State, vector<State>, BFSCmpState> queue;
        queue.push(t_state);

        while (queue.size() < m_omp_bfs_depth * m_threads_num) {
            State curr_state = queue.top();
            queue.pop();
            curr_state.explore(queue);
        }

        if (m_threads_num) omp_set_num_threads(m_threads_num);
#pragma omp parallel for schedule(dynamic)
        for (int i = queue.size(); i > 0; --i) {
            State state;

#pragma omp critical
            {
                state = queue.top();
                queue.pop();
            }

            iterativeDFSSolve(state);
        }
    } else {
        if (m_threads_num) omp_set_num_threads(m_threads_num);
#pragma omp parallel
        {
#pragma omp single
            recursiveDFSSolve(t_state);
        }
    }
    m_duration = high_resolution_clock::now() - start;
}

void
Solver::recursiveDFSSolve(State &t_state) {
    using namespace std;

    priority_queue<State, vector<State>, CmpState> queue;
    t_state.explore(queue);

    while (!queue.empty()) {
        State curr_state = queue.top();
        queue.pop();

#pragma omp task if (curr_state.getDepth() <= m_max_task_depth)
        recursiveDFSSolve(curr_state);
    }
}

void
Solver::iterativeDFSSolve(State &t_state) {
    using namespace std;

    priority_queue<State, vector<State>, DFSCmpState> stack;
    stack.push(t_state);

    while (!stack.empty()) {
        State curr_state = stack.top();
        stack.pop();
        curr_state.explore(stack);
    }
}

std::chrono::duration<double>
Solver::getDuration() const {
    return m_duration;
}