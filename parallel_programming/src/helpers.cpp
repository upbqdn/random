//
// Created by marek on 3/4/18.
//

#include "helpers.hpp"

void getFile(const std::string t_file_name, std::ifstream &t_file) {
    using namespace std;

    t_file.exceptions(ifstream::failbit | ifstream::badbit);

    try {
        t_file.open(t_file_name);
    } catch (ifstream::failure &readErr) {
        throw NO_FILE;
    }

    t_file.exceptions(ifstream::goodbit);
}

void printResults(Solution t_sol, std::chrono::duration<double> t_duration,
                  Params t_params) {
    using namespace std;

    for (auto const &move: t_sol) {
        cout << move.first.first << "-" << move.first.second;
        if (move.second) {
            cout << "*";
        }
        cout << "  ";
    }
    cout << endl;

    cout << "Moves: " << t_sol.size() - 1 << endl
         << "Threads: " << t_params.threads_num << endl
         << "BFS size: " << t_params.omp_bfs_depth * t_params.threads_num
         << endl
         << "Recursion max depth: " << t_params.task_depth << endl
         << "File: " << t_params.file_name << endl
         << "Duration: " << durationToString(t_duration) << endl
         << endl;
}

std::string
durationToString(std::chrono::duration<double> t_duration) {
    using namespace std;
    using namespace std::chrono;

    string out = "";

    auto s = duration_cast<seconds>(t_duration);
    auto ms = duration_cast<milliseconds>(t_duration -= s);
    auto us = duration_cast<microseconds>(t_duration -= ms);
    auto ns = duration_cast<nanoseconds>(t_duration -= us);

    if (s.count()) out += to_string(s.count()) + " s, ";
    if (ms.count()) out += to_string(ms.count()) + " ms, ";
    if (us.count()) out += to_string(us.count()) + " us, ";
    if (ns.count()) out += to_string(ns.count()) + " ns. ";

    return out;
}

void Params::parse(int t_argc, char **t_argv) {
    using namespace std;

    if (t_argc != ARGS_END) {
        throw WRONG_ARGS;
    }

    mpi_bfs_depth = atoi(t_argv[MPI_BFS_DEPTH]);
    omp_bfs_depth = atoi(t_argv[OMP_BFS_DEPTH]);
    threads_num = atoi(t_argv[THREADS_NUM]);
    task_depth = atoi(t_argv[TASK_DEPTH]);
    file_name = t_argv[FILE_PATH];

    if (!threads_num) {
        threads_num = omp_get_max_threads();
    }

    ifstream file;
    getFile(file_name, file);
    string line;
    getline(file, line);
    istringstream iss(line);
    iss >> board_size >> init_best_depth;

    uint16_t second = 0;

    while (getline(file, line)) {
        uint16_t first = 0;

        for (char field : line) {
            switch (field) {
                case '1':
                    rem_pieces.insert(make_pair(first, second));
                    break;
                case '2':
                    white_pieces.insert(make_pair(first, second));
                    break;
                case '3':
                    pos.first = first;
                    pos.second = second;
                    break;
                default:
                    break;
            }
            ++first;
        }

        ++second;
    }
}
