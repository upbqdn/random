(TeX-add-style-hook
 "report"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "a4wide"
    "float"
    "booktabs"
    "amsmath")
   (LaTeX-add-labels
    "seq-sol-sec"
    "exp-tab"))
 :latex)

