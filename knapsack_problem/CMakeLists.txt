cmake_minimum_required(VERSION 3.8)
project(knapsack_problem)

include_directories(include)

set(SOURCE_FILES src/main.cpp src/Item.cpp include/Item.hpp
        include/Constants.hpp src/Knapsack.cpp include/Knapsack.hpp
        src/Helpers.cpp include/Helpers.hpp)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "/home/marek/projects/knapsack_problem/workspace")

#adjust the following paths according to your system
include_directories(
        /usr/include/R/
        /usr/lib/R/library/RInside/include
        /usr/lib/R/library/Rcpp/include)

#adjust the following paths according to your system
link_directories(
        /usr/lib/R/library/RInside/lib
        /usr/lib/R/library/Rcpp/libs
        /usr/lib/R/lib)

add_executable(knapsack_problem ${SOURCE_FILES})

target_link_libraries(
        knapsack_problem
        RInside
        R
        Rcpp)