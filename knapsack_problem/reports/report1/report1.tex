\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\bibliographystyle{plain}

\usepackage{hyperref}
\title{The Knapsack Problem}

\author{Marek Bielik}

\date{\today}

\begin{document}
    \maketitle

    \section{Introduction}
    \label{sec:introduction}
    The Knapsack Problem \cite{wiki:knapsack} is an optimization problem which may be described in the following way: maximize $\sum_{i=1}^{n} v_i x_i$ subject to $\sum_{i=1}^{n} w_i x_i \leq W$, $x_i \in \{0,1\}$.
    Where $x_i$ represents the presence of an item in the knapsack, $v_i$ and $w_i$ are the price and weight of the item, respectively. $W$ represents the overall weight capacity of the knapsack.

    \section{Implementation description}
    \label{sec:implementation}
    The knapsack problem is known to be in the $\mathcal{NP}$ complexity class and may be solved via various techniques.
    This report focuses on using a brute force and a heuristic approach.
    Techniques such as dynamic programming or the branch and bound method will be covered in the next reports.

    \subsection{Brute force}
    The brute force solution is implemented as a recursive function.
    It traverses throughout the search space in a way similar to the depth-first search algorithm.
    We can imagine the search space as a binary tree with the depth $n$ which is the number of items that we consider.
    The leaf nodes of the tree, number of which is $n^2$, represent the particular solutions to the knapsack problem.
    The program visits each leaf node of the tree and returns the most optimal one.
    The time complexity is approximately $\mathcal{O}(2^{n+1})$ (we also consider the parent nodes in the tree) and the memory complexity is $\mathcal{O}(n)$.

    \subsection{Heuristic}
    The heuristic technique first sorts the items according to the ratio of the price to the weight of the items.
    The algorithm then gradually selects items with the best ratio until the overall weight reaches the limit.
    The time complexity of this approach (which mostly depends on the sorting algorithm) is approximately $\mathcal{O}((n \log n) + n)$ and the space complexity is $\mathcal{O}(n)$.

    \section{Results}
    Both implemented techniques were evaluated using test data which contain 50 instances of the knapsack problem with the same value of $n$.
    The resulting time values used in the following graphs are the average values per instance.
    All of the experiments were run on the Intel Core i5-5200U processor and the time was measured only during the necessary part of the algorithms (for example - loading of the test data is excluded as well as the preprocessing).

    Figure~\ref{fig-brute-force} depicts the time course of the brute force solution.
    We can see that the time course corresponds to the expected time complexity as the time seems to grow very slowly at the start and very steeply towards the end of the graph.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/brute_force}}
        \caption{The time course of the brute force method}
        \label{fig-brute-force}
    \end{figure}

    Figure~\ref{fig-brute-force-log} shows the same course but the y-axis is logarithmic.
    We can see that the time growth is actually exponential and that it grows very steeply from the very beginning.
    The reason why we don't see such a steep line at the start in the previous graph is because the instances with $n \approx 4$ are solved in hundreds of nanoseconds while the ones with $n \approx 32$ are solved in hundreds of seconds.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/brute_force_log}}
        \caption{The logarithmic time course of the brute force method}
        \label{fig-brute-force-log}
    \end{figure}

    Figure~\ref{fig-heuristic} depicts the time course for the heuristic approach.
    We can see that the time growth also approximately follows the theoretical complexity.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/heuristic}}
        \caption{The time course of the heuristic method}
        \label{fig-heuristic}
    \end{figure}

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccc@{}}
            \toprule
            Number of items & Avg. relative error [\%] & Max. relative error [\%] \\ \midrule
            4 & 7.15 & 68.4 \\
            10 & 3.09 & 15.8 \\
            15 & 0.97 & 8.54 \\
            20 & 1.39 & 8.43 \\
            22 & 1.57 & 8.75 \\
            25 & 1.33 & 5.42 \\
            27 & 1.07 & 10.6 \\
            30 & 1.07 & 5.51 \\
            32 & 0.76 & 3.61 \\
            35 & 1.04 & 4.61 \\
            37 & 0.77 & 8.2 \\
            40 & 0.66 & 2.57 \\ \bottomrule
        \end{tabular}
        \caption{Average and maximal relative error of the heuristic approach}
        \label{tab-heuristic}
    \end{table}

    Table~\ref{tab-heuristic} sums up the average and maximal relative error of the heuristic method.
    The values of the average relative error were calculated according to formula~\ref{eq-relative-error} for each of the 50 instances separately, summed up and averaged afterwards for the same value of $n$.

    \begin{equation}
        \frac{optimal Price - approximate Price}{optimal Price}
        \label{eq-relative-error}
    \end{equation}

    We can see that the average relative error is usually around 1\%.
    Other heuristic approaches (sorting according to the price and weight separately instead of their ratio) were also implemented and tested and it emerged that the results had much higher relative errors.
    There was also a 'sub-heuristic' approach implemented - when two or more items have the same ratio, they are further compared according to their price.
    It emerged that this additional approach had no effect on the final results at all.

    \section{Conclusion}
    The experiments proved the properties of the knapsack problem arising from the fact that it belongs to the $\mathcal{NP}$-hard complexity class.
    A brute force solution for 50 instances of the problem with $n=10$ required approximately \SI{1}{\milli\second}.
    A solution for $n=20$ required approximately \SI{1}{\second} and for $n=30$ approximately 18 minutes which is roughly \SI{1000}{\second}.
    We can see that the computation time increases about 1000 times when we increase $n$ by 10.
    We can assume that a solution for $n=40$ would take approximately 11 days and 31 years for $n=50$, for $n=60$ it would be 31000 years and so on.
    Another remarkable fact is that the computer would still need only a few kilobytes of memory for all of these computations.
    \bibliography{../bib/references}
\end{document}