\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\bibliographystyle{plain}
\usepackage{mathtools}
\usepackage{amsmath}

\usepackage{hyperref}
\title{The Knapsack Problem}

\author{Marek Bielik}

\date{\today}

\begin{document}
    \maketitle

    \section{Introduction}
    \label{sec:introduction}
    The Knapsack Problem \cite{wiki:knapsack} is an optimization problem which may be described in the following way: maximize $\sum_{i=1}^{n} v_i x_i$ subject to $\sum_{i=1}^{n} w_i x_i \leq W$, $x_i \in \{0,1\}$.
    Where $x_i$ represents the presence of an item in the knapsack, $v_i$ and $w_i$ are the price and weight of the item, respectively. $W$ represents the overall weight capacity of the knapsack.
    This version is called the 0/1 optimization knapsack problem.

    \section{Algorithms evaluation}
    \label{sec:evaluation}
    The version of the knapsack problem described above is known to be in the \mbox{$\mathcal{NP}$-hard} complexity class and may be solved via various techniques.

    This report focuses on all the methods discussed in the previous reports, that is: the brute force method, simple heuristic, the branch \& bound method, dynamic programming and the fully polynomial-time approximation scheme.
    Each method was examined separately by generating various knapsack instances.
    The default parameters for the knapsack generator are summarized in table~\ref{tab-gen}.

    During each experiment, only one parameter was changed.
    The following sections describe the parameters that the implemented methods are sensitive to.
    It is important to note that the parameter n (the number of items that we consider) was set to 40 in all of the experiments.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}cc@{}}
            \toprule
            parameter & value \\ \midrule
            n & 40 \\
            N & 20 \\
            m & 0.5 \\
            W & 1000 \\
            C & 1000 \\
            k & 1 \\
            d & 0 \\ \bottomrule
        \end{tabular}
        \caption{Default parameters for the knapsack generator}
        \label{tab-gen}
    \end{table}

    \subsection{Brute force}
    The brute force method has very long computation time even for $n = 32$ and was thoroughly examined in the first report.
    Therefore, there were no further experiments carried out.

    \subsection{Simple heuristic}
    Experiments showed that the relative error of this method was sensitive to the parameter m.
    In table~\ref{tab-heuristic-err}, we can see that both the average and maximal error decrease as the parameter m increases.
    That means that the algorithm is more accurate for knapsacks with higher capacity and exact for knapsacks into which we can put all the items.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccc@{}}
            \toprule
            parameter & \multicolumn{2}{c}{relative error [\%]} \\
            m & max & avg \\ \midrule
            0.1 & 14.7 & 3.03 \\
            0.2 & 6.00 & 2.66 \\
            0.3 & 6.86 & 2.19 \\
            0.4 & 5.40 & 1.72 \\
            0.5 & 3.17 & 1.29 \\
            0.6 & 2.77 & 0.93 \\
            0.7 & 2.01 & 0.83 \\
            0.8 & 1.64 & 0.44 \\
            0.9 & 0.75 & 0.13 \\
            1.0 & 0.00 & 0.00 \\ \bottomrule
        \end{tabular}
        \caption{Average and maximal error of the heuristic method in relation to the parameter m}
        \label{tab-heuristic-err}
    \end{table}

    \subsection{Branch \& bound}
    The runtime of this method evinced sensitivity to parameters m and k.
    In figure~\ref{fig-branch-bound-m}, we can see that the execution time was highest at the value 0.4 of parameter m.
    Therefore, we can assume that this method is most efficient when we can put only a few items into the knapsack or all of them.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/brute_force_m}}
        \caption{The runtime dependency of the branch \& bound method on parameter~m}
        \label{fig-branch-bound-m}
    \end{figure}

    Figure~\ref{fig-branch-bound-k} shows the runtime dependency on parameter k (paramter d was set to -1).
    We can see that the runtime is higher for lower values of k which means that the algorithm runs faster for instances with a higher ratio of light items and vice versa.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/brute_force_k}}
        \caption{The runtime dependency of the branch \& bound method on parameter~k (parameter d was set to -1)}
        \label{fig-branch-bound-k}
    \end{figure}

    \subsection{Dynamic programming}
    This method uses decomposition according to the value of the items and figure~\ref{fig-dyn-prog} shows its pseudo-polynomial character.
    We can see that the runtime increases linearly with the parameter C, which means that the time complexity linearly grows as the maximal value of items increases.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/dyn_prog.pdf}}
        \caption{The runtime dependency of the dynamic programming method on parameter~C}
        \label{fig-dyn-prog}
    \end{figure}

    \subsection{Fully polynomial-time approximation scheme}
    This approach rounds the maximal value; and therefore it is not dependent on it.
    Figure~\ref{fig-fptas} shows that the runtime doesn't grow when the maximal value increases.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/fptas}}
        \caption{The runtime dependency of FPTAS on parameter~C}
        \label{fig-fptas}
    \end{figure}

    \section{Conclusion}
    The presented experiments showed that the execution time and also the relative (whether the maximal or average) error of the implemented algorithms are both dependent not only on the size of the instance but also on its properties and each of the algorithms has different sensitivity to these properties.

    There were many experiments carried out and some of them showed only very vague results.
    Therefore, only the experiments with evident results were taken into consideration.

    \bibliography{../bib/references}
\end{document}