\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\bibliographystyle{plain}
\usepackage{mathtools}
\usepackage{tablefootnote}
\usepackage{hyperref}

\title{The Knapsack Problem}

\author{Marek Bielik}

\date{\today}

\begin{document}
    \maketitle

    \section{Introduction}
    \label{sec:introduction}
    The Knapsack Problem \cite{wiki:knapsack} is an optimization problem which may be described in the following way: maximize $\sum_{i=1}^{n} v_i x_i$ subject to $\sum_{i=1}^{n} w_i x_i \leq W$, $x_i \in \{0,1\}$.
    Where $x_i$ represents the presence of an item in the knapsack, $v_i$ and $w_i$ are the price and weight of the item, respectively. $W$ represents the overall weight capacity of the knapsack.
    This version is called the 0/1 optimization knapsack problem.

    \section{Simulated Annealing}
    \label{sec:sim-annealing}
    The version of the knapsack problem described above is known to be in the \mbox{$\mathcal{NP}$-hard} complexity class and may be solved via various techniques.

    This report focuses on the simulated annealing technique.
    Not only the parameters of the simulated annealing algorithm are crucial to the effective functionality but also the choice of the energy (goal) function, that evaluates the solutions, and the candidate generator procedure, that generates new (neighbour) states from the current ones.

    The value of the energy function is simply the sum of the prices of the items in the knapsack.

    The knapsack is represented as a vector of bits (items) and the candidate generator procedure randomly flips one bit.
    If the weight of the newly generated neighbour solution (knapsack) exceeds the capacity, it is considered invalid and the algorithm continues without evaluating this solution.

    The algorithm tries multiple candidate solutions at the same temperature and the number of the attempts is set by the parameter $k$.
    The initial temperature is set by the parameter $T$ and the cooling coefficient is set by the parameter $a$.

    \section{Experiments}
    The parameters $k$, $T$ and $a$ described in the previous section were examined using 50 instances of the knapsack problem with $n = 40$.
    The initial values were set to $k = 10$, $T = 100$ and $a = 0.9$ and during each experiment, only one parameter was examined.

    In table~\ref{tab-temperature}, we can see that the efficiency of the algorithm doesn't improve anymore when the temperature exceeds 100; and therefore, the most optimal initial temperature $T$ was set to 100.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccc@{}}
            \toprule
            $T$ & ARE\tablefootnote[1]{\label{note-are}ARE - Average Relative Error} [\%] & DPS\tablefootnote[2]{\label{note-dps}DPS- Duration Per Instance} [\SI{}{\micro\second}]\\ \midrule
            1 & 75.2 & 7\\
            10 & 20.9 & 58\\
            100 & 8.99 & 90\\
            1000 & 7.69 & 162 \\
            10000 & 7.3 & 220\\
            100000 & 7.5 & 204 \\
            1000000 & 7.44 & 222 \\ \bottomrule
        \end{tabular}
        \caption{The average relative error and duration dependency on the temperature parameter $T$.}
        \label{tab-temperature}
    \end{table}

    Table~\ref{tab-alpha} contains the results of experiments with the parameter $a$.
    We can see that as the value of the parameter gets closer to 1, the ARE\footnotemark[1] decreases and the computation time increases (approximately linearly).

    We can see a very similar course in table \ref{tab-k} which contains results of experiments with the parameter $k$.
    Both parameters $a$ and $k$ have similar impact on the overall performance of the algorithm and they can be used interchangeably~-~we can increase only one of the parameters and we get similar results as if we increased the other one (we also get a similar computation time).

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccc@{}}
            \toprule
            $a$ & ARE\footnotemark[1] [\%] & duration [\SI{}{\micro\second}]\\ \midrule
            0.1 & 42.8 & 12\\
            0.5 & 22.4 & 19\\
            0.9 & 9.64 & 91\\
            0.99 & 1.93 & 950 \\
            0.999 & 0.263 & 8316\\
            0.9999 & 0.06 & 72203\\
            0.99999 & 0.04 & 718000\\ \bottomrule
        \end{tabular}
        \caption{The average relative error and duration dependency on the cooling parameter $a$.}
        \label{tab-alpha}
    \end{table}

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccc@{}}
            \toprule
            $k$ & ARE\footnotemark[1] [\%] & duration [\SI{}{\micro\second}]\\ \midrule
            1 & 21 & 15\\
            10 & 8.54 & 92\\
            50 & 2.68 & 487\\
            100 & 1.55 & 1333\\
            1000 & 0.32 & 7372 \\
            10000 & 0.07 & 68581\\
            100000 & 0.04 & 667351\\ \bottomrule
        \end{tabular}
        \caption{The average relative error and duration dependency on the parameter $k$.}
        \label{tab-k}
    \end{table}

    Another interesting result was that both parameters $a$ and $k$ shift the course of the optimization towards the highest temperature (where the algorithm starts).
    Figure~\ref{fig-evolution1} depicts the course of the optimization with $a = 0.95$, $k = 1$ and $T = 100$.
    We can see that as the temperature decreases, the price of the solution increases approximately linearly.
    However, the average relative error of the solutions with these values of the parameters was 20.8 \%.
    Figure~\ref{fig-evolution2} shows the course of the optimization with $a = 0.999$, $k = 50$, $T = 100$.
    We can see that the course is skewed and shifted towards the initial temperature and that the price increases very slowly as the temperature decreases.
    The average relative error of the solutions with this setup was only 0.08\% though.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/evolutionCourse1}}
        \caption{The course of the optimization, $a = 0.95$, $k = 1$, $T = 100$.}
        \label{fig-evolution1}
    \end{figure}

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/evolutionCourse2}}
        \caption{The course of the optimization, $a = 0.999$, $k = 50$, $T = 100$.}
        \label{fig-evolution2}
    \end{figure}

    \section{Conclusion}
    The experiments demonstrated that the 0/1 optimization knapsack problem can be effectively solved using the simulated annealing approach.
    The most effective combination of the parameters (regarding the relative average error and computation time) was set to $a = 0.999$, $k = 50$ and $T = 100$.
    The average relative error with this set up was 0.08\% and the average computation time for 1 instance with $n = 40$ was \SI{36.45}{\milli\second}.

    The algorithm wasn't able to decrease the average relative error under 0.04\% even with very high values of the parameters when it took minutes to solve one instance.

    \bibliography{../bib/references}
\end{document}