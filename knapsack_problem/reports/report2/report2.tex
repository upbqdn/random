\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\bibliographystyle{plain}
\usepackage{mathtools}
\usepackage{amsmath}

\usepackage{hyperref}
\title{The Knapsack Problem}

\author{Marek Bielik}

\date{\today}

\begin{document}
    \maketitle

    \section{Introduction}
    \label{sec:introduction}
    The Knapsack Problem \cite{wiki:knapsack} is an optimization problem which may be described in the following way: maximize $\sum_{i=1}^{n} v_i x_i$ subject to $\sum_{i=1}^{n} w_i x_i \leq W$, $x_i \in \{0,1\}$.
    Where $x_i$ represents the presence of an item in the knapsack, $v_i$ and $w_i$ are the price and weight of the item, respectively. $W$ represents the overall weight capacity of the knapsack.
    This version is called the 0/1 optimization knapsack problem.

    \section{Implementation Description}
    \label{sec:implementation}
    The version of the knapsack problem described above is known to be in the \mbox{$\mathcal{NP}$-hard} complexity class and may be solved via various techniques.

    This report focuses on the branch \& bound method, dynamic programming and on the fully polynomial-time approximation scheme (FPTAS).

    \subsection{Branch \& bound}
    This method is an optimized version of the brute-force solution that was discussed in the previous report.

    The brute force solution was implemented as a recursive function.
    It traversed throughout the search space (binary tree with the depth $n$, which is the number of items that we consider, and with $n^2$ of leaf nodes which are the particular solutions to the knapsack problem) in a way similar to the depth-first search algorithm.
    The program visited each leaf node and returned the most optimal solution.

    The branch \& bound method uses two optimization techniques of the previous approach:

    \begin{enumerate}
        \item If the maximal capacity is exceeded in the current node, don't examine the child nodes and go back.
        \item If it is not possible to get a better solution in the following branches of the current node, don't examine the child nodes and go back.
    \end{enumerate}

    The time and space complexity remains the same as the complexity of the original brute force method because we have to consider the worst case scenario when the algorithm has to visit each node in the tree.
    Therefore, the time complexity is approximately $\mathcal{O}(2^{n+1})$ (we also consider the parent nodes in the tree) and the memory complexity is $\mathcal{O}(n)$.

    \subsection{Dynamic programming}
    The dynamic programming solution to the knapsack problem implemented for this report uses decomposition according to the price of the items.
    It would also be possible the decompose the problem according to the weight, but this approach would not be suitable for the FPTAS solution.

    The algorithm uses a bottom-up approach - first it calculates the sum of all the items that are available, then it starts with 0 items and gradually determines the best solution up to $n$ items using the previous solutions with $n-1$ items.

    The algorithm runs in pseudo-polynomial time $\mathcal{O}(Pn)$.
    The runtime depends on the number of items $n$ and the sum of the prices of the items $P$.
    The space complexity can be described in an analogous way.

    \subsection{Fully polynomial-time approximation scheme}
    This approach makes use of the dynamic programming solution.
    Since we are able to solve the knapsack problem in pseudo-polynomial time, we can make the algorithm truly polynomial by restricting the size of the input.
    We can restrict the size by rounding off the least significant digits of the price values.
    The process is described in the following formulas:

    \begin{flalign}
        & & &\epsilon \in (0, 1 \rangle& \\
        & & &P := max \{ v_i \mid 1 \leq i \leq n \}& \\
        & & &K := \epsilon \frac{P}{n}& \\
        & & & v_i' := \lfloor \frac{v_i}{K} \rfloor &
        \label{eq-fptas}
    \end{flalign}

    The knapsack problem is then solved by dynamic programming with the new items' values $v_i'$ with the relative maximal error less than $\epsilon$.

    \section{Results}
    All the implemented techniques were evaluated using test data which contain 50 instances of the knapsack problem with the same value of $n$.
    The resulting time values used in the following graphs are the average values per instance.

    All of the experiments were run on the Intel Core i5-5200U processor and the time was measured only during the necessary part of the algorithms (for example - loading of the test data is excluded as well as the preprocessing).

    Figure~\ref{fig-brute-force} depicts the time course of the branch \& bound solution.
    The first optimization technique didn't have such a significant impact on the overall duration than the second one.

    We can see that the time course corresponds to the expected time complexity as the time seems to grow very slowly at the start and very steeply towards the end of the graph.

    In every instance set, there was at least one instance which had the most optimal solution in the very last leaf of the search tree so the algorithm could not apply any optimizations and had to scour the whole tree.
    The solution was to sort the items first according to their price and then run the actual algorithm.
    This caused the most optimal solution to be the first one that was tried so the algorithm terminated instantly.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/brute_force}}
        \caption{The time course of the branch \& bound method}
        \label{fig-brute-force}
    \end{figure}

    Figure~\ref{fig-brute-force-log} shows the same course but the y-axis is logarithmic.
    We can see that the time growth is actually exponential and that it grows very steeply from the very beginning.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/brute_force_log}}
        \caption{The logarithmic time course of the branch \& bound method}
        \label{fig-brute-force-log}
    \end{figure}

    Figure~\ref{fig-dyn-prog} depicts the time course of the dynamic programming solution.
    We can see that the time growth is not exponential anymore and that it approximately follows the expected time complexity.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/dyn_prog}}
        \caption{The time course of the dynamic programming solution}
        \label{fig-dyn-prog}
    \end{figure}

    Figure~\ref{fig-fptas} shows the time course of FPTAS.
    The value of $\epsilon$ was set to 0.9 and we can see that the algorithm solves the instances faster than the dynamic programming approach and that it is also polynomial.
    It also emerged that lower $\epsilon$ values caused higher runtime of the algorithm.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/fptas}}
        \caption{The time course of FPTAS}
        \label{fig-fptas}
    \end{figure}

    Table~\ref{tab-fptas} sums up the maximal relative errors.
    The values of the errors were calculated according to formula~\ref{eq-relative-error} for each instance separately.

    We can see that the maximal relative error is usually under 1\% and as the value of $\epsilon$ decreases, the error comes closer to zero.
    We can also notice that instances with lower $n$ have higher error.

    \begin{equation}
        \frac{optimal Price - approximate Price}{optimal Price}
        \label{eq-relative-error}
    \end{equation}

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccc@{}}
            \toprule
            Number of items & \multicolumn{2}{c}{Max. relative error [\%]} \\
            & $\epsilon = 0.9$ & $\epsilon = 0.2$ \\ \midrule
            4 & 14.8 & 1.63 \\
            10 & 2.38 & 1.08 \\
            15 & 1.29 & 0.18 \\
            20 & 0.41 & 0.04 \\
            22 & 0.38 & 0.03 \\
            25 & 0.31 & 0.03 \\
            27 & 0.41 & 0.03 \\
            30 & 0.29 & 0.05 \\
            32 & 0.20 & 0.05 \\
            35 & 0.14 & 0.00 \\
            37 & 0.12 & 0.02 \\
            40 & 0.27 & 0.02 \\ \bottomrule
        \end{tabular}
        \caption{maximal relative errors of FPTAS}
        \label{tab-fptas}
    \end{table}

    \section{Conclusion}
    The experiments proved the properties of the 0/1 optimization knapsack problem arising from the fact that it belongs to the $\mathcal{NP}$-hard complexity class.

    The branch \& bound method (especially its second optimization) significantly decreased the runtime even though the asymptotic time complexity is still exponential.
    The previous report showed that the solution for a set of 50 instances with $n=30$ required approximately 18 minutes while the branch \& bound method with pre-sorted items solved the same set in \SI{3}{\milli\second}.
    However, if we increased $n$ up to a hundred, we would not be able to solve the problem anymore again (this also depends on the position of the best solution in the search tree).

    The dynamic programming solution provided us a tool for solving the problem in pseudo-polynomial time while we still obtain the exact solution.
    This method also run much faster than the brute-force method.
    The set of instances discussed above was solved in \SI{200}{\milli\second}.

    If we restrict the size of the input by rounding off the least significant digits of the price of the items, we get a fully polynomial-time approximation scheme.
    This approach only provides approximate solutions and we could see that they did not significantly differ from the exact solutions.
    Even when we set $\epsilon$ to 0.9, the maximal relative error was 0.29\% and the required amount of time was \SI{27}{\milli\second} for the set of 50 instances with $n=30$.

    \bibliography{../bib/references}
\end{document}