//
// Created by marek on 10/14/17.
//

#ifndef KNAPSACK_PROBLEM_CONSTANTS_HPP
#define KNAPSACK_PROBLEM_CONSTANTS_HPP

#include <string>
#include <vector>

namespace knapsack {
    static const std::string NO_FILE{"Could not open the input file."};
    static const double EPS{0.000001};
    static const double FPTAS_EPS{0.9};
    static const std::vector<unsigned> FILE_NAMES{/*4,10,15,20,22,25,27,
                                                       30,32,35,37,40*/
            40};
    static const unsigned DEFAULT_MAX_ITEMS{27};
    static const std::string FILE_PREFIX{"/knap_"};
    static const std::string FILE_SUFFIX{".inst.dat"};
}

#endif //KNAPSACK_PROBLEM_CONSTANTS_HPP
