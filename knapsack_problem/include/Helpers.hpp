//
// Created by marek on 10/18/17.
//

#include <vector>

#ifndef KNAPSACK_PROBLEM_HELPERS_HPP
#define KNAPSACK_PROBLEM_HELPERS_HPP

namespace helpers {
    enum DurationType {
        BRUTE_FORCE = 0,
        SIM_ANNEALING,
        DURATION_COUNT
    };


    std::string
    durationToString(std::chrono::duration<double> t_duration);

    void
    printResults(std::vector<std::chrono::duration<double>> &t_durations,
                 const std::vector<double> &t_rel_err);

    void plotGraphs(std::vector<std::vector<double>> t_params,
                    std::vector<std::vector<double>> t_durations);

    void
    plotEvolution(const std::vector<int> &t_price,
                  const std::vector<double> &t_temperature);

    void getFile(std::string t_file_name, /*out*/ std::ifstream &t_file);
}

#endif //KNAPSACK_PROBLEM_HELPERS_HPP
