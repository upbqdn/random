//
// Created by marek on 10/14/17.
//

#ifndef KNAPSACK_PROBLEM_ITEM_HPP
#define KNAPSACK_PROBLEM_ITEM_HPP

namespace knapsack {
    class Item {
    public:
        Item(unsigned t_weight, unsigned t_price);

        const unsigned getWeight() const;

        const unsigned getPrice() const;

        const bool operator>(const Item &t_item) const;

        const double evaluate() const;

        static const bool priceCompare(const Item &lhs, const Item &rhs);

        const bool isInserted() const;

        void takeOut();

        void insert();

        const bool swap();

    protected:
        bool m_inserted;
        unsigned m_weight,
                m_price;
        double m_rating;
    };
}

#endif //KNAPSACK_PROBLEM_ITEM_HPP
