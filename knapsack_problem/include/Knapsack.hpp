//
// Created by marek on 10/15/17.
//

#ifndef KNAPSACK_PROBLEM_KNAPSACK_HPP
#define KNAPSACK_PROBLEM_KNAPSACK_HPP


#include <sstream>
#include <vector>
#include <chrono>
#include "Item.hpp"

namespace knapsack {

    enum Attribute {
        weight,
        price
    };

    class Knapsack {
    public:
        Knapsack(std::string t_instance_description, double t_epsilon);

        unsigned int bruteForceSolve(bool t_optimization = false);

        unsigned int simAnnealingSolve();

        unsigned int heuristicSolve();

        unsigned int dynProgSolve();

        unsigned int fptasSolve();

        const unsigned int getOptimalPrice() const;

        const std::chrono::duration<double> getDuration() const;

        const unsigned int getID() const;

        friend std::ostream &
        operator<<(std::ostream &t_os, Knapsack const &t_knapsack);

        unsigned int getItemsCount() const;

    protected:
        void recursiveSolve(unsigned t_recursion_depth, unsigned t_weight,
                            unsigned t_price);
        const unsigned int getOverallPrice(std::vector<Item> &t_items) const;

        unsigned dynProg(std::vector<Item> &t_items);

        unsigned m_id,
                m_items_count,
                m_capacity,
                m_best_price;

        bool m_optimization = false;
        std::chrono::duration<double> m_duration;
        std::vector<Item> m_items;
        std::vector<Item> m_rounded_items;
        std::vector<Item> m_sorted_items;
        std::vector<unsigned> m_potential_price;
    };

}

#endif //KNAPSACK_PROBLEM_KNAPSACK_HPP
