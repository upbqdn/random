//
// Created by marek on 10/14/17.
//

#include <cmath>
#include <Constants.hpp>
#include "Item.hpp"

namespace knapsack {
    Item::Item(unsigned t_weight, unsigned t_price)
            : m_weight(t_weight),
              m_price(t_price),
              m_rating(m_price / static_cast<double>(t_weight)),
              m_inserted(false) {};

    const unsigned Item::getWeight() const {
        return m_weight;
    }

    const unsigned Item::getPrice() const {
        return m_price;
    }

    const bool Item::operator>(const Item &t_item) const {
        using namespace std;

        auto lhs = this->evaluate();
        auto rhs = t_item.evaluate();

        if (abs(lhs - rhs) < EPS) {
            if (this->m_price > t_item.getPrice()) return false;
            if (this->m_price < t_item.getPrice()) return true;
        }

        return lhs > rhs;
    }

    const double Item::evaluate() const {
        return m_rating;
    }

    const bool
    Item::priceCompare(const Item &lhs, const Item &rhs) {
        return lhs.m_price > rhs.m_price;
    }

    const bool Item::isInserted() const {
        return m_inserted;
    }

    void Item::takeOut() {
        m_inserted = false;
    }

    void Item::insert() {
        m_inserted = true;
    }

    const bool Item::swap() {
        return m_inserted = !m_inserted;
    }
}