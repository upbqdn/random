//
// Created by marek on 10/15/17.
//

#include "Knapsack.hpp"
#include <iostream>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <random>
#include "Helpers.hpp"

namespace knapsack {
    Knapsack::Knapsack(std::string t_instance_description, double t_epsilon) {
        using namespace std;

        istringstream iss(t_instance_description);
        iss >> m_id >> m_items_count >> m_capacity;
        m_items.reserve(m_items_count);
        m_rounded_items.reserve(m_items_count);
        m_potential_price.reserve(m_items_count);
        unsigned weight, price;

        while (iss >> weight && iss >> price) {
            m_items.push_back(Item(weight, price));
        }

        m_sorted_items = m_items;
        sort(m_sorted_items.begin(), m_sorted_items.end(), &Item::priceCompare);

        for (int i = m_sorted_items.size() - 1, cumulative_price = 0;
             i >= 0; --i) {
            cumulative_price += m_sorted_items[i].getPrice();
            m_potential_price[i] = cumulative_price;
        }

        double k = t_epsilon * (m_sorted_items[0].getPrice() / m_items_count);

        for (auto const &i : m_items) {
            auto rounded_price = static_cast<unsigned>(floor(i.getPrice() / k));
            Item rounded_item(i.getWeight(), rounded_price);
            m_rounded_items.push_back(rounded_item);
        }
    }

    unsigned int Knapsack::bruteForceSolve(bool t_optimization) {
        using namespace std::chrono;
        m_best_price = 0;
        m_optimization = t_optimization;

        auto start = high_resolution_clock::now();
        recursiveSolve(0, 0, 0);
        m_duration = high_resolution_clock::now() - start;

        return m_best_price;
    }

    unsigned int Knapsack::heuristicSolve() {
        using namespace std;
        using namespace std::chrono;

        m_best_price = 0;
        auto items = m_items;
        auto overall_weight = 0;

        auto start = high_resolution_clock::now();

        sort(items.begin(), items.end(), greater<>());
        for (auto const &item: items) {
            if ((overall_weight += item.getWeight()) > m_capacity) {
                break;
            }
            m_best_price += item.getPrice();
        }

        m_duration = high_resolution_clock::now() - start;

        return m_best_price;
    }

    void Knapsack::recursiveSolve(unsigned t_recursion_depth, unsigned t_weight,
                                  unsigned t_price) {
        if (t_recursion_depth >= m_items_count) {
            if (t_weight <= m_capacity && t_price > m_best_price) {
                m_best_price = t_price;
            }
            return;
        }

        auto items_weight =
                t_weight + m_sorted_items[t_recursion_depth].getWeight();

        if (m_optimization) {
            if (t_price + m_potential_price[t_recursion_depth] < m_best_price) {
                return;
            }

            if (items_weight <= m_capacity) {
                recursiveSolve(t_recursion_depth + 1, items_weight,
                               t_price +
                               m_sorted_items[t_recursion_depth].getPrice());
            }
        } else {
            recursiveSolve(t_recursion_depth + 1, items_weight,
                           t_price +
                           m_sorted_items[t_recursion_depth].getPrice());
        }

        recursiveSolve(t_recursion_depth + 1, t_weight, t_price);

    }

    const unsigned int Knapsack::getOptimalPrice() const {
        return m_best_price;
    }

    const std::chrono::duration<double> Knapsack::getDuration() const {
        return m_duration;
    }

    const unsigned int Knapsack::getID() const {
        return m_id;
    }

    std::ostream &operator<<(std::ostream &t_os, Knapsack const &t_knapsack) {
        using namespace helpers;
        using namespace std;

        t_os << "ID: " << t_knapsack.getID()
             << " items: " << t_knapsack.getItemsCount()
             << " price: " << t_knapsack.getOptimalPrice()
             << " duration: " << durationToString(t_knapsack.m_duration);
        t_os << endl;

        return t_os;
    }

    unsigned int Knapsack::getItemsCount() const {
        return m_items_count;
    }

    unsigned int Knapsack::dynProgSolve() {
        return dynProg(m_items);
    }

    unsigned int Knapsack::fptasSolve() {
        return dynProg(m_rounded_items);
    }

    const unsigned int
    Knapsack::getOverallPrice(std::vector<Item> &t_items) const {
        using namespace std;

        return static_cast<unsigned>
        (accumulate(begin(t_items), end(t_items), 0,
                    [](unsigned i, const Item &item) {
                        return item.getPrice() + i;
                    }));
    }

    unsigned Knapsack::dynProg(std::vector<Item> &t_items) {
        using namespace std;
        using namespace std::chrono;

        m_best_price = 0;
        unsigned overall_price = getOverallPrice(t_items);
        vector<vector<unsigned>> w(m_items_count + 1,
                                   vector<unsigned>(overall_price + 1));

        auto start = high_resolution_clock::now();

        for (int n = 0; n <= m_items_count; n++) {
            for (int p = 0; p <= overall_price; p++) {
                if (p == 0) {
                    w[n][p] = 0;
                } else if (n == 0) {
                    w[n][p] = numeric_limits<unsigned>::max() / 2;
                } else if (t_items[n - 1].getPrice() <= p) {
                    w[n][p] = min(t_items[n - 1].getWeight() +
                                  w[n - 1][p - t_items[n - 1].getPrice()],
                                  w[n - 1][p]);
                } else {
                    w[n][p] = w[n - 1][p];
                }
            }
        }

        m_duration = high_resolution_clock::now() - start;

        int p = overall_price;

        while (w[m_items_count][p] > m_capacity) {
            p--;
        };

        for (int n = m_items_count; n > 0; --n) {
            if (w[n][p] != w[n - 1][p]) {
                m_best_price += m_items[n - 1].getPrice();
                p = p - t_items[n - 1].getPrice();
            }
        }

        return m_best_price;
    }

    unsigned int Knapsack::simAnnealingSolve() {
        using namespace std;
        using namespace std::chrono;
        using namespace helpers;

        double alpha = 0.999999;
        int max_k = 5000;
        unsigned long long init_T = 10000;

        m_best_price = 0;
        int cur_w = 0;
        int cur_p = 0;

        vector<int> price_evolution;
        vector<double> temperature_evolution;

        mt19937_64 rng;
        long seed = high_resolution_clock::now().time_since_epoch().count();
        seed_seq ss{uint32_t(seed & 0xffffffff), uint32_t(seed >> 32)};
        rng.seed(ss);
        uniform_real_distribution<double> unif(0, 1);

        auto start = high_resolution_clock::now();
        for (double cur_T = init_T; cur_T >= 1; cur_T *= alpha) {
            for (int k = 1; k <= max_k; ++k) {
                ulong i = rng() % m_items.size();
                bool i_added = m_items[i].swap();
                int i_w = m_items[i].getWeight();
                int i_p = m_items[i].getPrice();

                if (i_added && ((cur_w + i_w) > m_capacity)) {
                    m_items[i].swap();
                    continue;
                }

                if (i_added) {
                    cur_w += i_w;
                    cur_p += i_p;
                    if (cur_p > m_best_price) {
                        m_best_price = cur_p;
                        price_evolution.push_back(m_best_price);
                        temperature_evolution.push_back(cur_T);
                    }
                } else {
                    if (unif(rng) < exp(-i_p / cur_T)) {
                        cur_w -= i_w;
                        cur_p -= i_p;
                    } else {
                        m_items[i].swap();
                    }
                }
            }
        }
        m_duration = high_resolution_clock::now() - start;

       // plotEvolution(price_evolution, temperature_evolution);

        return m_best_price;
    }
}