#include <iostream>
#include <fstream>
#include <Item.hpp>
#include <Knapsack.hpp>
#include "Constants.hpp"
#include "Helpers.hpp"

int main(int argc, char **argv) {
    using namespace knapsack;
    using namespace std;
    using namespace std::chrono;
    using namespace helpers;

    bool optimization = true;
    unsigned long max_items = DEFAULT_MAX_ITEMS;

    if (argc >= 3) {
        max_items = stoul(argv[2]);
    }

    vector<vector<double>> durations(DURATION_COUNT);

    for (int i = 0; i < FILE_NAMES.size(); i++) {
        ifstream input_file;

        try {
            getFile(argv[1] + FILE_PREFIX + to_string(FILE_NAMES[i]) +
                    FILE_SUFFIX, input_file);
        } catch (const invalid_argument &e) {
            cout << e.what();
            return EXIT_FAILURE;
        }

        string instance_description;
        vector<duration<double>> experiment_durations(DURATION_COUNT);
        vector<double> rel_err;

        while (getline(input_file, instance_description)) {
            Knapsack knapsack(instance_description, 0.5);

            auto optimal_sol = knapsack.bruteForceSolve(optimization);
            experiment_durations[BRUTE_FORCE] += knapsack.getDuration();

            auto sim_annealing_sol = knapsack.simAnnealingSolve();
            experiment_durations[SIM_ANNEALING] += knapsack.getDuration();

            double heuristic_numerator = optimal_sol - sim_annealing_sol;
            rel_err.push_back(heuristic_numerator / optimal_sol);
        }

        input_file.close();

        for (auto &duration: experiment_durations) {
            duration /= rel_err.size();
        }

        printResults(experiment_durations, rel_err);

        durations[BRUTE_FORCE].push_back(duration_cast<milliseconds>(
                experiment_durations[BRUTE_FORCE]).count());

        if (FILE_NAMES[i] == max_items) {
            break;
        }
    }

    //plotGraphs(durations);

    return EXIT_SUCCESS;
}