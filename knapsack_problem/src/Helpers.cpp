//
// Created by marek on 10/18/17.
//

#include <chrono>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <RInside.h>
#include "Helpers.hpp"
#include "Constants.hpp"

namespace helpers {
    std::string
    durationToString(std::chrono::duration<double> t_duration) {
        using namespace std;
        using namespace std::chrono;

        string out = "";

        auto m = duration_cast<minutes>(t_duration);
        auto s = duration_cast<seconds>(t_duration -= m);
        auto ms = duration_cast<milliseconds>(t_duration -= s);
        auto us = duration_cast<microseconds>(t_duration -= ms);
        auto ns = duration_cast<nanoseconds>(t_duration -= us);

        if (m.count()) out += to_string(m.count()) + " m, ";
        if (s.count()) out += to_string(s.count()) + " s, ";
        if (ms.count()) out += to_string(ms.count()) + " ms, ";
        if (us.count()) out += to_string(us.count()) + " us, ";
        if (ns.count()) out += to_string(ns.count()) + " ns. ";

        return out;
    }

    void
    printResults(std::vector<std::chrono::duration<double>> &t_durations,
                 const std::vector<double> &t_rel_err) {
        using namespace std;
        cout << "Average brute-force duration: "
             << durationToString(t_durations[BRUTE_FORCE]) << endl
             << "Average SA duration: "
             << durationToString(t_durations[SIM_ANNEALING]) << endl
             << setprecision(3)
             << "Average relative error for SA: "
             <<
             accumulate(t_rel_err.begin(), t_rel_err.end(), 0.0) /
             t_rel_err.size() * 100.0 << '%' << endl
             << "Maximal relative error for SA: "
             << *max_element(t_rel_err.begin(), t_rel_err.end()) * 100.0 << '%'
             << endl << endl;
    }

    void plotGraphs(std::vector<std::vector<double>> t_params,
                    std::vector<std::vector<double>> t_durations) {
        using namespace std;

        RInside R;
        R["items"] = t_params[0];
        string path = "graphs";

        R["time"] = t_durations[BRUTE_FORCE];
        string cmd = "pdf('" + path + "/brute_force.pdf'); "
                "plot(items, time, type='l', col='green', xaxt='n',"
                "     ylab='Time [ms] per instance', xlab='Parameter k', lwd=2);"
                "axis(1, at = items, las=2);"
                "abline(v = items, col='lightgray', lty='dotted');"
                "abline(h = time, col='lightgray', lty='dotted');"
                "legend('topleft', inset=c(0.28,-0.1), xpd=TRUE,"
                "       legend=c('Branch & bound duration'),"
                "       lty=c(1), lwd=c(2), col=c('green'));"
                "dev.off();";

        R.parseEvalQ(cmd);
    }

    void
    plotEvolution(const std::vector<int> & t_price,
                  const std::vector<double> & t_temperature) {
        using namespace std;

        static RInside R;
        R["price"] = t_price;
        string path = "graphs";

        R["temperature"] = t_temperature;
        string cmd = "pdf('" + path + "/evolutionCourse.pdf'); "
                "plot(temperature, price, col='green',"
                "     ylab='Price of the solution', xlab='Temperature');"
                "legend('topleft', inset=c(0.28,-0.1), xpd=TRUE,"
                "       legend=c('Solution evolution'),"
                "       lty=c(1), lwd=c(1), col=c('green'));"
                "dev.off();";

        R.parseEvalQ(cmd);
    }

    void getFile(const std::string t_file_name, std::ifstream &t_file) {
        using namespace std;
        using namespace knapsack;

        t_file.exceptions(ifstream::failbit | ifstream::badbit);

        try {
            t_file.open(t_file_name);
        } catch (ifstream::failure &readErr) {
            throw invalid_argument(NO_FILE);
        }

        t_file.exceptions(ifstream::goodbit);
    }
}
