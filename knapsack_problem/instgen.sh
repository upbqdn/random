#!/bin/bash

test=0;
rm -rf workspace/instances/
n=40;
N=20;
m=0.5;
W=1000;
C=1000;
k=0.1;
d=-1;

for i in {1..10}; do
    path="workspace/instances/";
    mkdir -p ${path};

    echo "${n} ${N} $m $W ${C} $k $d" >> ${path}knap_${i}.inst.dat;
    ./knapgen/knapgen -n ${n} -N ${N} -m ${m} -W ${W} -C ${C} -k ${k} -d ${d} 2>> ${path}error.log >> ${path}knap_${i}.inst.dat;

    k=$(echo "$k + 0.1" | bc -l)
done

echo "Instances generated.";