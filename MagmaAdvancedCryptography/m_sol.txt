//// begin program /////

// q is a random prime up to 160 bits long
q := RandomPrime(160: Proof := false);

// p is a random prime, up to 1024 bits long and congruent to 1 mod q
// p = 1 (mod q) 
// this allows q | p - 1
repeat l, p := RandomPrime(1024, 1, q, 1000 : Proof := false);
    until l;

// o = p - 1 is the order of the multiplicative group Z_p
o := p - 1;

// r is a random integer up to p
r := Random(p);

// n = o / q
n := o div q;

// g cannot be 1, otherwise we would have to get a different value of r
// (it is very unlikely for g to be 1)
g := Modexp(r, n, p);

print "g:";
g;

// the order of g, o(g) is q
// so g is a generator of a subgroup of Z_p, the order of which is q
// the result of g^q (mod p) is 1
// since g = r^n = r^(o / q) (mod p), therefore
// g^q = r^(n*q) = r^(o / q * q) = r^o = 1 (mod p)

print "\ng^q (mod p):";
Modexp(g, q, p);

a := Random(o);
//A = g^a (mod p)
A := Modexp(g, a, p);

b := Random(o);
//B = g^b (mod p)
B := Modexp(g, b, p);

//A_s = B^a = g^(b * a) = g^(a * b) = A^b = B_s (mod p)
A_s := Modexp(B, a, p);
B_s := Modexp(A, b, p);

print "\nDo Alice and Bob share the same secret?";
A_s eq B_s;

print "\nThe shared secret is:";
A_s;

//// end program ////

///// begin output ////

g:
1399083985587246823922331152914572256602436560300410605774400390056586752611592\
2413798387358018703046045895845133645731411431477989957938946276205874789380682\
2630576600589216363750726721738603019168717502194835029203210468261915902285187\
2820658199660251979419146192605516299706193442466453585884022848231265

g^q (mod p):
1

Do Alice and Bob share the same secret?
true

The shared secret is:
1927903150257395135952994900951378994564927513825115084007691402938407461254441\
1817640611027960489750149658851926731889257474310227584812759508511175359124135\
6848747976042822052738119943596791090001335905007490070510458948956837138111379\
1948934468117656861733888003090761255988829394149997889559820171529192

//// end output /////

//// begin program /////

// length of the modulus
L := 152;

// RSA exponent
e := 3;

// RSA moduli
// EulerPhi(n_i) is coprime to e,
// the number of moduli needs to be equal or greater than e
n := [RSAModulus(L, e), RSAModulus(L, e), RSAModulus(L, e)];

// plaintext of the size up to the smallest modulus
p := Random(Minimum(n));

// ciphertexts
// c_i = p^e (mod n_i)
c := [Modexp(p, e, n[1]), Modexp(p, e, n[2]), Modexp(p, e, n[3])];

// By the CRT, there is a unique common value x which is congruent to 
// each c_i (mod n_i): x = c_i (mod n_i).
// x can be obtained in the following way:
// x = SUM(c_i * N_i * N_i') (mod N),
// where
// N = the product of all n_i,
// N_i = N / n_i, in fact N_j is a multiple of n_i for i != j,
// N_i' is the modular multiplicative inverse of N_i (mod n_i).
// In the code, we obtain x by calling CRT(c, n).
// Since c_i = p^e (mod n_i), by the CRT, also x = p^e (mod  N).
// Since p is smaller than any n_i, we have p^e < N.
// Therefore, x = p^e.

print "Is the cube root of x equal to our plaintext?";
Iroot(CRT(c, n), e) eq p;

//// end program ////

///// begin output ////

Is the cube root of x equal to our plaintext?
true

//// end output /////

//// begin program /////

n := Random(2^100);
print "random number of length up to 100 bit:";
n;
print "factors:";
time Factorization(n);

n := Random(2^150);
print "\nrandom number of length up to 150 bit:";
n;
print "factors:";
time Factorization(n);

n := Random(2^200);
print "\nrandom number of length up to 200 bit:";
n;
print "factors:";
time Factorization(n);

n := Random(2^144);
print "\nrandom number of length up to 144 bit:";
n;
print "factors:";
time Factorization(n);

n := Random(2^171);
print "\nrandom number of length up to 171 bit:";
n;
print "factors:";
time Factorization(n);

n := Random(2^202);
print "\nrandom number of length up to 202 bit:";
n;
print "factors:";
time Factorization(n);

//// end program ////

///// begin output ////

random number of length up to 100 bit:
340062624610295694941433831934
factors:
[ <2, 1>, <3229, 1>, <12659, 1>, <3625229, 1>, <1147429513547693, 1> ]
Time: 0.010

random number of length up to 150 bit:
1271217374598194850370935970414055264464237054
factors:
[ <2, 1>, <73, 1>, <193, 1>, <449, 1>, <673, 1>, <911, 1>, <112153, 1>,
<5003123, 1>, <292063844729157335851, 1> ]
Time: 0.010

random number of length up to 200 bit:
876534636893108673618786100886248048769899477792766232166676
factors:
[ <2, 2>, <73, 1>, <337276253181008887, 1>,
<8900214349372094413724004468384870465419, 1> ]
Time: 1.650

random number of length up to 144 bit:
20978488679905105949729367737335002669109478
factors:
[ <2, 1>, <1583, 1>, <6626180884366742245650463593599179617533, 1> ]
Time: 0.010

random number of length up to 171 bit:
2239046364496757724424053485049737845946754080242281
factors:
[ <3, 2>, <29, 1>, <839, 1>, <15859159, 1>, <3904701006790896101, 1>,
<165117338952423418321, 1> ]
Time: 0.960

random number of length up to 202 bit:
2215784771535430212709235870450415413430719638040906221852748
factors:
[ <2, 2>, <23, 1>, <223, 1>, <95261, 1>, <7895511495607, 1>,
<143595052409947505488271346315551586689, 1> ]
Time: 0.100

//// end output /////
