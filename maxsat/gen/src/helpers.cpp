//
// Created by marek on 12/29/17.
//
#include <dirent.h>
#include <string>
#include <vector>
#include "helpers.hpp"

namespace maxsat {
    std::vector<std::string> getFileNames(std::string t_path) {
        using namespace std;
        DIR *dir;
        dirent *pdir;
        vector<string> files;

        dir = opendir(t_path.c_str());

        while (pdir = readdir(dir)) {
            string name = pdir->d_name;
            if (name != "." && name != "..") {
                files.push_back(pdir->d_name);
            }
        }

        return files;
    }

}