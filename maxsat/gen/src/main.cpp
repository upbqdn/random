//
// Created by marek on 12/29/17.
//

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include "helpers.hpp"
#include "constants.hpp"
#include <random>
#include <chrono>
#include <cstring>

int main() {
    using namespace maxsat;
    using namespace std;
    using namespace std::chrono;

    static mt19937_64 rng;
    static long seed = high_resolution_clock::now().time_since_epoch().count();
    static seed_seq ss{uint32_t(seed & 0xffffffff), uint32_t(seed >> 32)};
    rng.seed(ss);
    static uniform_int_distribution<int> unif(MIN, MAX);

    vector<string> file_names = getFileNames(INST_PATH);

    for (const auto &file_name : file_names) {
        ofstream file;
        file.open(INST_PATH + file_name, ios::out | ios::app);

        for (int i = 1; i <= FILE_COUNT; ++i) {
            file << unif(rng);
            if (i != FILE_COUNT) {
                file << ' ';
            }
        }
        file.close();
    }

    return 0;
}

