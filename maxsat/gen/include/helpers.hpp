//
// Created by marek on 12/29/17.
//

#ifndef MAXSAT_HELPERS_HPP
#define MAXSAT_HELPERS_HPP

namespace maxsat {
    std::vector<std::string> getFileNames(std::string t_path);
}

#endif //MAXSAT_HELPERS_HPP
