//
// Created by marek on 12/29/17.
//

#ifndef MAXSAT_CONSTANTS_HPP
#define MAXSAT_CONSTANTS_HPP

#include <string>
#include <vector>

namespace maxsat {
    static const std::string INST_PATH{
            "/home/marek/projects/maxsat/instances/uf100-430/"};
    static const int MIN{1};
    static const int MAX{300};
    static const int FILE_COUNT{50};
}

#endif //MAXSAT_CONSTANTS_HPP
