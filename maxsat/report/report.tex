\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\bibliographystyle{plain}
\usepackage{mathtools}
\usepackage{tablefootnote}
\usepackage{hyperref}

\title{Weighted Maximum Satisfiability Problem}

\author{Marek Bielik}

\date{\today}

\begin{document}
    \maketitle

    \section{Introduction}
    \label{sec:introduction}
    The weighted maximum satisfiability problem (WMSP) \cite{wiki:maxsat} is an optimization problem which may be described in the following way: given a conjunctive normal form formula with non-negative weights assigned to each variable, find truth values that maximize the combined weight of the variables that are set to true so that the whole formula is satisfied.

    \section{Simulated Annealing}
    \label{sec:sim-annealing}
    The WMSP described above is known to be in the \mbox{$\mathcal{NP}$-hard} complexity class and may be solved via various techniques.

    This report focuses on simulated annealing.
    Not only the parameters of the algorithm are crucial to its effective functionality but also the choice of the energy (goal) function, that evaluates the solutions, and the candidate generator procedure, that generates new (neighbour) states from the current ones.

    The energy function is described in formula \ref{eq:energy}.
    The function $s(\vec{x})$ is an auxiliary function that returns the number of satisfied clauses in the formula.
    $\vec{x}$ is a vector of the variables and $w$ is the weight of a variable.

    \begin{equation}
        \label{eq:energy}
        E(\vec{x}) = \sum_{i} w_{i} + s(\vec{x}) \cdot \sum w , \quad i \in \{ i|x_i = 1 \}
    \end{equation}

    The energy function is used to compare the candidate solutions among each other and it makes sure that:

    \begin{enumerate}
        \item every valid solution has a better value than any invalid solution,
        \item invalid solutions are compared by their level of satisfiability,
        \item invalid solutions with the same level of satisfiability are compared by their weight,
        \item valid solutions are compared by their weight.
    \end{enumerate}

    The above four points are necessary for the algorithm to work correctly.

    The variables are represented as a vector of bits and the candidate generator procedure randomly flips one bit.
    The reason for this is that with simulated annealing, we need to make neighbour states as close in energy as possible and this can be achieved by flipping exactly one variable.
    The initial state is a vector with randomly assigned variable values.

    The algorithm tries multiple candidate solutions at the same temperature and the number of the attempts is set by the parameter $k$ (some authors refer to this parameter as an \textit{equilibrium}).
    The \textit{initial temperature} is set by the parameter $T$ and the \textit{cooling-down coefficient} is set by the parameter $a$.

    \section{Experiments}
    The parameters $k$, $T$ and $a$ described in the previous section were examined using 50 instances of WMSP, each having 50 variables with 218 clauses and 100 variables with 430 clauses.
    Every variable had a randomly assigned weight between 1 and 500 (uniform distribution).
    All of the experiments were implemented in C++17 and run on the Intel Core i5-5200U processor under Linux v4.14.
    The time was measured only during the necessary part of the algorithm (for example - loading of the test data is excluded as well as the preprocessing).

    \subsection{Determining the most effective parameters}
    The initial values were set to $k = 5$, $T = 100$ and $a = 0.9$, and during each experiment, only one parameter was examined.
    There were no solved instances for such a amount of variables, so the energy function had to be evaluated only relatively - it was measured in \%.
    100\% would mean that all the variables in the formula are set to 1 (this gives the maximal possible weight of the formula) and also every clause is satisfied, which means that the whole formula is satisfied (with all variables set to 1, this is the maximal possible energy state of the instance).
    The AE\footnotemark[2] column in the following tables is the Average Energy of all the formulas in the experiment.

    In table~\ref{tab-temperature}, we can see that the efficiency of the algorithm doesn't improve anymore when the temperature exceeds 100~000, and even 10~000 provides similar results, so the most optimal initial temperature $T$ was set to 100~000.
    We can see that the duration grows linearly as the temperature increases.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccccccc@{}}
            \toprule
            & \multicolumn{3}{c}{50 variables, 218 clauses} & \multicolumn{3}{c}{100 variables, 430 clauses} \\
            \cmidrule(lr){2-4} \cmidrule(lr){5-7}
            $T$ & SF\tablefootnote{\label{note-sf}SF - satisfied formulas} [\%] & AE\tablefootnote[2]{\label{note-e}AE - Average Energy} [\%] & ADPI\tablefootnote[3]{\label{note-dpi}ADPI - Average Duration Per Instance} [\SI{}{\milli\second}] & SF\footnotemark[1] [\%] & AE\footnotemark[2] [\%] & ADPI\footnotemark[3] [\SI{}{\milli\second}]  \\
            \midrule
            10 & 0 & 97.2 & 2.98 & 0 & 95.1 & 6.21 \\
            100 & 0 & 97.9 & 5.97 & 0 & 96.9 & 17.1\\
            1 000 & 4 & 98.1 & 10.6 & 0 & 97.5 & 20.3\\
            10 000 & 6 & 98.6 & 12.9 & 0 & 97.9 & 25.1\\
            100 000 & 2 & 98.6 & 16.4 & 0 & 98.2 & 32.3\\
            100 0000 & 4 & 98.5 & 19.1 & 0 & 98.1 & 39.3\\
            \bottomrule
        \end{tabular}
        \caption{Experiments for determining the most optimal initial temperature $T$.}
        \label{tab-temperature}
    \end{table}

    Table \ref{tab-cooling-down} shows the impact of the cooling-down parameter $a$.
    The percentage of satisfied formulas grows as the value of $a$ increases, but the average energy does not improve after $a$ reaches 0.995.
    At value 0.999, the computation time increased approximately 6-times, but there was hardly any improvement in the efficiency of the algorithm and therefore the most optimal value of $a$ was set to 0.995.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccccccc@{}}
            \toprule
            & \multicolumn{3}{c}{50 variables, 218 clauses} & \multicolumn{3}{c}{100 variables, 430 clauses} \\
            \cmidrule(lr){2-4} \cmidrule(lr){5-7}
            $a$ & SF\footnotemark[1] [\%] & AE\footnotemark[2] [\%] & ADPI\footnotemark[3] [\SI{}{\milli\second}] & SF\footnotemark[1] [\%] & AE\footnotemark[2] [\%] & ADPI\footnotemark[3] [\SI{}{\second}]  \\
            \midrule
            0.7 & 0 & 95.2 & 1.83 & 0 & 93.1 & 0.01 \\
            0.9 & 2 & 97.9 & 6.97 & 0 & 96.8 & 0.01 \\
            0.95 & 4 & 98.1 & 12.5 & 0 & 98.0 & 0.03\\
            0.99 & 2 & 98.1 & 63.5 & 0 & 98.4 & 0.13\\
            0.995 & 8 & 98.8 & 112 & 0 & 98.7 & 0.22\\
            0.999 & 14 & 98.7 & 629 & 4 & 98.9 & 1.3\\
            \bottomrule
        \end{tabular}
        \caption{Experiments for determining the most optimal cooling-down parameter $a$.}
        \label{tab-cooling-down}
    \end{table}

    Table~\ref{tab-equilibrium} contains results for the parameter $k$.
    We can see that the parameter has no influence on the percentage of satisfied formulas and that the average energy doesn't significantly improve after the value of $k$ reaches 20.
    Therefore, the most effective value of $k$ was set to 20.
    The computation time grows linearly.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}ccccccc@{}}
            \toprule
            & \multicolumn{3}{c}{50 variables, 218 clauses} & \multicolumn{3}{c}{100 variables, 430 clauses} \\
            \cmidrule(lr){2-4} \cmidrule(lr){5-7}
            $k$ & SF\footnotemark[1] [\%] & AE\footnotemark[2] [\%] & ADPI\footnotemark[3] [\SI{}{\milli\second}] & SF\footnotemark[1] [\%] & AE\footnotemark[2] [\%] & ADPI\footnotemark[3] [\SI{}{\milli\second}]  \\
            \midrule
            1 & 0 & 94.3 & 1.16 & 0 & 91.9 & 2.35  \\
            3 & 0 & 97.2 & 3.44 & 0 & 95.5 & 6.91  \\
            5 & 2 & 97.8 & 6.40 & 0 & 96.9 & 11.2  \\
            10 & 2 & 98.1 & 13.4 & 0 & 97.7 & 22.2 \\
            20 & 0 & 98.2 & 27.3 & 0 & 98.3 & 43.8 \\
            50 & 2 & 98.2 & 65.7 & 0 & 98.5 & 106 \\
            100 & 2 & 98.4 & 115 & 0 & 98.6 & 233 \\
            \bottomrule
        \end{tabular}
        \caption{Experiments for determining the most optimal parameter $k$ for the equilibrium.}
        \label{tab-equilibrium}
    \end{table}

    \section{Results}
    We could see in the previous experiments that by rising the values of the parameters individually, there was a threshold where only the computation time kept increasing and the efficiency of the algorithm wasn't improving.
    We could also see that the algorithm wasn't able to satisfy more than 14 \% of the formulas (out of 50 instances with 50 variables and 218 clauses) and the usual result was even around 2 \%.

    \subsection{Overall performance}
    Let's look at the most optimal parameters combined together, table~\ref{tab-best-results} sums up the results.
    The percentage of the satisfied formulas is much higher in the both previously examined data-sets and also the average energy is significantly increased.
    However, the computation time is significantly increased as well.
    The table also contains the average percentage of satisfied clauses (SC\footnotemark[4]) in every formula.
    The resulting numbers are similar to the average energy, this is because of the way the energy function is designed - it highly depends on the amount of satisfied clauses, this pushes the simulated annealing algorithm towards satisfied formulas.

    We can see that the algorithm satisfied every formula with 20 variables (the amout of satified clauses is also 100 \%).
    However, the algorithm wasn't able to satisfy any formula with 200 variables even though the average amount of satisfied clauses was 99.6 \%.
    This means that the algorithm was always very close to a fully satisfied formula, but there was always at least one clause missing.
    This reveals the nature of the problem being \mbox{$\mathcal{NP}$-hard}, meaning that the search space grows exponentially and even advanced techniques such as simulated annealing are still sensitive to the size of the instances.

    \begin{table}[H]
        \centering
        \begin{tabular}{@{}cccccc@{}}
            \toprule
            variables & clauses & SF\footnotemark[1] [\%] & AE\footnotemark[2] [\%] & ADPI\footnotemark[3] [\SI{}{\second}] & SC\tablefootnote[4]{\label{note-sc}SC - Satisfied Clauses} [\%] \\
            \midrule
            20 & 91 & 100 & 99.5 & 0.51 & 100 \\
            50 & 218 & 62 & 99.61 & 1.41 & 99.8 \\
            100 & 430 & 28 & 99.6 & 2.53 & 99.7 \\
            200 & 860 & 0 & 99.63 & 4.9 & 99.6 \\
            \bottomrule
        \end{tabular}
        \caption{Results of experiments with the most optimal parameters: $T = 100 000$, $a = 0.995$, $k = 20$. }
        \label{tab-best-results}
    \end{table}

    \subsection{Evolution of the solutions}
    In order to better find out how the algorithm performs in finding the most optimal solution, there were generated graphs that contain the value of the energy function at a certain temperature.
    These help to depict the course of the optimization.
    One graph contains 5 separate runs of the algorithm with an instance with 50 variables and 218 clauses.

    In figure~\ref{fig-course-best}, we can see the course with the optimal parameters from the previous section: $T = 100 000$, $a = 0.995$, $k = 20$.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/course-best}}
        \caption{The course of the optimization with the most optimal parameters, $a = 0.995$, $k = 20$, $T = 100 000$.}
        \label{fig-course-best}
    \end{figure}

    An interesting result was that the algorithm was sensitive to the ratio between the initial temperature and the other two parameters $a$ and $k$ (these two parameters seemed to work interchangeably).
    Increasing the parameters $a$ and $k$ (either separately or both of them) shifted the course of the optimization towards the initial temperature (where the algorithm starts).
    Also, decreasing the temperature had the same effect on the course.
    That means that the ratio was important.
    We can see this behaviour in figure~\ref{fig-evolution2} where the initial temperature was lowered to 10~000.
    The algorithm progresses very fast at the start and then makes only a very little progress.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/skewed-course1}}
        \caption{The course of the optimization, $a = 0.995$, $k = 20$, $T = 10 000$.}
        \label{fig-evolution2}
    \end{figure}

    The opposite course can be seen in figure~\ref{fig-evolution3}.
    Parameters $a$ and $k$ were lowered and the algorithm was progressing mostly at the end.

    \begin{figure}[H]
        \centerline{\includegraphics[scale=0.6]{figures/skewed-course2}}
        \caption{The course of the optimization, $a = 0.9$, $k = 5$, $T = 100 000$.}
        \label{fig-evolution3}
    \end{figure}

    \section{Conclusion}
    The goal of this report was to demonstrate the capabilities of simulated annealing to solve the weighted maximum satisfiability problem.
    The most effective combination of the parameters was set to $a = 0.995$, $k = 20$ and $T = 100~000$.

    There were no solved instances of this problem since the brute-force approach was not feasible.
    For this reason, the solutions were evaluated only relatively.
    It was shown that the combination of appropriate parameters that were experimentally determined produced much better results than the high values of the parameters individually.
    Instances with 20 variables and 91 clauses were all satisfied, whilst none of the instances with 200 variables and 860 clauses were satisfied.
    However, the percentage of satisfied clauses was still 99.6 \%.

    Close attention was also paid to the evolution process of the algorithm.
    Graphs showed that there is a relation between the initial temperature and the parameters $a$ and $k$ that was examined in the previous section.\\

    \bibliography{bib/references}
\end{document}