//
// Created by marek on 1/10/18.
//

#include <sstream>
#include "Clause.hpp"
#include "constants.hpp"

maxsat::Clause::Clause(std::string t_description) {
    using namespace std;

    stringstream iss(t_description);
    for (int i = 0; i < LITERAL_COUNT; ++i) {
        int literal;
        iss >> literal;
        m_literals.push_back(literal);
    }
}

const bool
maxsat::Clause::isSatisfied(const std::vector<bool> &t_variables) const {
    for (const auto &literal : m_literals) {
        if (literal > 0) {
            if (t_variables[literal - 1]) {
                return true;
            }
        } else {
            if (!t_variables[abs(literal) - 1]) {
                return true;
            }
        }
    }

    return false;
}

