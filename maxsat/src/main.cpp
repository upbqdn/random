#include <iostream>
#include <vector>
#include <fstream>
#include "helpers.hpp"
#include <Formula.hpp>
#include "constants.hpp"
#include <Solver.hpp>

int main(int argc, char *argv[]) {
    using namespace maxsat;
    using namespace std;
    using namespace std::chrono;

    if (argc < 7) {
        exit(EXIT_FAILURE);
    }

    string dir = INST_PATH + argv[DIRECTORY] + "/";
    vector<string> file_names = getFileNames(dir);
    vector<unsigned> sat_formula_clauses;
    vector<double> sol_values;
    vector<bool> sat_formulas;
    duration<double> duration;
    unsigned cls_cnt;
    Solver solver(atof(argv[ALPHA]), atoi(argv[K]), atoi(argv[INIT_T]),
                  (bool) argv[GRAPHS], atoi(argv[REPS]));

    for (int i = 0; i < file_names.size(); ++i) {
        const auto file_name = file_names[i];
        ifstream file;
        getFile(dir + file_name, file);
        Formula formula(file);
        file.close();

        sol_values.push_back(
                (double) solver.solve(formula) / formula.getMaxVal());
        sat_formula_clauses.push_back(solver.getSatClsCnt());
        sat_formulas.push_back(solver.isSatisfied());
        duration += solver.getDuration();
        cls_cnt = formula.getClsCnt();

        cout << i << ": satisfied clauses: " << sat_formula_clauses[i]
             << " satisfied: " << sat_formulas[i]
             << " energy: " << sol_values[i] * 100 << "%" << endl;
    }

    cout << "SC: " << (double) accumulate(sat_formula_clauses.begin(),
                                          sat_formula_clauses.end(), 0) /
                      sat_formula_clauses.size() / cls_cnt * 100 << "%" << endl
         << "SF: "
         << (double) accumulate(sat_formulas.begin(), sat_formulas.end(), 0) /
            sat_formulas.size() * 100 << "%" << endl
         << "AVG DPS: " << durationToString(duration / sat_formulas.size())
         << endl
         << "AVG energy: "
         << accumulate(sol_values.begin(), sol_values.end(), 0.0) /
            sol_values.size() * 100 << "%" << endl;

    return 0;
}