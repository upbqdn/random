//
// Created by marek on 1/10/18.
//

#include <dirent.h>
#include <string>
#include <vector>
#include "helpers.hpp"
#include "constants.hpp"
#include <fstream>
#include <iomanip>
#include <RInside.h>

namespace maxsat {
    std::vector<std::string> getFileNames(std::string t_path) {
        using namespace std;
        DIR *dir;
        dirent *pdir;
        vector<string> files;

        dir = opendir(t_path.c_str());

        while (pdir = readdir(dir)) {
            string name = pdir->d_name;
            if (name != "." && name != "..") {
                files.push_back(pdir->d_name);
            }
        }

        return files;
    }

    void getFile(const std::string t_file_name, std::ifstream &t_file) {
        using namespace std;

        t_file.exceptions(ifstream::failbit | ifstream::badbit);

        try {
            t_file.open(t_file_name);
        } catch (ifstream::failure &readErr) {
            throw invalid_argument(NO_FILE);
        }

        t_file.exceptions(ifstream::goodbit);
    }

    std::string
    durationToString(std::chrono::duration<double> t_duration) {
        using namespace std;
        using namespace std::chrono;

        string out = "";

        auto s = duration_cast<seconds>(t_duration);
        auto ms = duration_cast<milliseconds>(t_duration -= s);
        auto us = duration_cast<microseconds>(t_duration -= ms);
        auto ns = duration_cast<nanoseconds>(t_duration -= us);

        if (s.count()) out += to_string(s.count()) + " s, ";
        if (ms.count()) out += to_string(ms.count()) + " ms, ";
        if (us.count()) out += to_string(us.count()) + " us, ";
        if (ns.count()) out += to_string(ns.count()) + " ns. ";

        return out;
    }

    std::mt19937_64 rng(
            std::chrono::system_clock::now().time_since_epoch().count());

    void
    plotEvolution(const std::vector<unsigned> &t_energy,
                  const std::vector<double> &t_temperature) {
        using namespace std;

        static RInside R;
        static unsigned cnt = 1;
        R["price"] = t_energy;
        R["temperature"] = t_temperature;
        string cmd =
                "pdf('" + GRAPHS_PATH + "/sol" + to_string(cnt) + ".pdf');"
                        "plot(temperature, price, col='green',"
                        "     ylab='Value of the solution', xlab='Temperature');"
                        "legend('topleft', inset=c(0.28,-0.1), xpd=TRUE,"
                        "       legend=c('Solution evolution'),"
                        "       lty=c(1), lwd=c(1), col=c('green'));"
                        "dev.off();";

        R.parseEvalQ(cmd);
        ++cnt;
    }
}