//
// Created by marek on 1/19/18.
//

#include <helpers.hpp>
#include "Solver.hpp"

namespace maxsat {
    maxsat::Solver::Solver(const double t_alpha, const int t_max_k,
                           const unsigned long long int t_init_T,
                           const bool t_print_graph,
                           const unsigned t_reps) :
            m_alpha(t_alpha),
            m_max_k(t_max_k),
            m_init_T(t_init_T),
            m_plot_graph(t_print_graph),
            m_reps(t_reps) {
    }

    unsigned maxsat::Solver::solve(maxsat::Formula &t_formula) {
        using namespace std;
        using namespace std::chrono;

        uniform_real_distribution<double> unif(0.0, 1.0);
        vector<unsigned> energies;
        vector<double> temperatures;

        for (int i = 0; i < m_reps; ++i) {
            m_best_sol = 0;
            t_formula.randomInit();
            int cur_sol = t_formula.evaluate();
            energies.push_back(cur_sol);
            temperatures.push_back(m_init_T);

            const auto start = high_resolution_clock::now();
            for (double cur_T = m_init_T; cur_T >= 1; cur_T *= m_alpha) {
                for (int k = 1; k <= m_max_k; ++k) {
                    ulong i = t_formula.changeState();
                    int new_sol = t_formula.evaluate();
                    int diff = new_sol - cur_sol;

                    if (diff > 0 || unif(rng) < exp(diff / cur_T)) {
                        cur_sol = new_sol;
                        if (cur_sol > m_best_sol) {
                            energies.push_back(m_best_sol = (unsigned) cur_sol);
                            temperatures.push_back(cur_T);
                            m_sat_cls_cnt = t_formula.getSatClsCnt();
                            m_satisfied = t_formula.getSatClsCnt() ==
                                          t_formula.getClsCnt();
                        }
                        continue;
                    } else {
                        t_formula.returnChange(i);
                    }
                }
            }
            m_duration = high_resolution_clock::now() - start;
        }

        if (m_plot_graph) {
            plotEvolution(energies, temperatures);
        }

        return m_best_sol;
    }

    const bool Solver::isSatisfied() const {
        return m_satisfied;
    }

    std::chrono::duration<double> Solver::getDuration() const {
        return m_duration;
    }

    const unsigned Solver::getSatClsCnt() const {
        return m_sat_cls_cnt;
    }

}