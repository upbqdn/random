//
// Created by marek on 1/10/18.
//

#include <sstream>
#include "helpers.hpp"
#include "Formula.hpp"

namespace maxsat {
    Formula::Formula(std::ifstream &t_file) {
        using namespace std;
        using namespace std::chrono;

        string line;

        while (getline(t_file, line)) {
            if (*line.begin() == 'p') {
                istringstream iss(line);
                string skip_dummy;
                iss >> skip_dummy >> skip_dummy >> m_variable_cnt
                    >> m_cls_cnt;
                break;
            }
        }

        for (int i = 0; i < m_cls_cnt; ++i) {
            getline(t_file, line);
            m_clauses.emplace_back(line);
        }

        while (getline(t_file, line)) {
            if (line.empty()) {
                break;
            }
        }

        getline(t_file, line);
        istringstream iss(line);
        m_max_weight = 0;

        for (int i = 0; i < m_variable_cnt; ++i) {
            unsigned weight;
            iss >> weight;
            m_weights.push_back(weight);
            m_max_weight += weight;
        }

        m_max_val = m_max_weight + m_cls_cnt * m_max_weight;

        m_variables.resize(m_variable_cnt);
    }

    unsigned Formula::getWeight() {
        unsigned weight = 0;

        for (int i = 0; i < m_variable_cnt; ++i) {
            if (m_variables[i]) {
                weight += m_weights[i];
            }
        }

        return weight;
    }

    unsigned int Formula::evaluate() {
        m_sat_cls_cnt = 0;

        for (const auto &clause : m_clauses) {
            if (clause.isSatisfied(m_variables)) {
                ++m_sat_cls_cnt;
            }
        }

        return getWeight() + m_sat_cls_cnt * m_max_weight;
    }

    ulong Formula::changeState() {
        ulong i = rng() % m_variable_cnt;
        m_variables[i] = !m_variables[i];
        return i;
    }

    void Formula::returnChange(ulong t_i) {
        m_variables[t_i] = !m_variables[t_i];
    }

    const unsigned Formula::getMaxVal() const {
        return m_max_val;
    }

    const unsigned Formula::getSatClsCnt() const {
        return m_sat_cls_cnt;
    }

    const unsigned Formula::getClsCnt() const {
        return m_cls_cnt;
    }

    void Formula::randomInit() {
        m_sat_cls_cnt = 0;
        for (int i = 0; i < m_variable_cnt; ++i) {
            m_variables[i] = rng() % 2 != 0;
        }
    }
}