# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/marek/projects/maxsat/src/Clause.cpp" "/home/marek/projects/maxsat/cmake-build-debug/CMakeFiles/maxsat.dir/src/Clause.cpp.o"
  "/home/marek/projects/maxsat/src/Formula.cpp" "/home/marek/projects/maxsat/cmake-build-debug/CMakeFiles/maxsat.dir/src/Formula.cpp.o"
  "/home/marek/projects/maxsat/src/Solver.cpp" "/home/marek/projects/maxsat/cmake-build-debug/CMakeFiles/maxsat.dir/src/Solver.cpp.o"
  "/home/marek/projects/maxsat/src/helpers.cpp" "/home/marek/projects/maxsat/cmake-build-debug/CMakeFiles/maxsat.dir/src/helpers.cpp.o"
  "/home/marek/projects/maxsat/src/main.cpp" "/home/marek/projects/maxsat/cmake-build-debug/CMakeFiles/maxsat.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/include/R"
  "/usr/lib/R/library/RInside/include"
  "/usr/lib/R/library/Rcpp/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
