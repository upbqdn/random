//
// Created by marek on 1/10/18.
//

#ifndef MAXSAT_HELPERS_HPP
#define MAXSAT_HELPERS_HPP

#include <random>
#include <chrono>

namespace maxsat {
    extern std::mt19937_64 rng;

    std::vector<std::string> getFileNames(std::string t_path);

    void getFile(std::string t_file_name, /*out*/ std::ifstream &t_file);

    std::string
    durationToString(std::chrono::duration<double> t_duration);

    void
    plotEvolution(const std::vector<unsigned> &t_energy,
                  const std::vector<double> &t_temperature);
}
#endif //MAXSAT_HELPERS_HPP
