//
// Created by marek on 1/10/18.
//

#ifndef MAXSAT_FORMULA_HPP
#define MAXSAT_FORMULA_HPP

#include <fstream>
#include <vector>
#include <random>
#include <chrono>
#include "Clause.hpp"

namespace maxsat {
    class Formula {
    public:
        Formula(std::ifstream &t_file);

        unsigned getWeight();

        const unsigned getMaxVal() const;

        const bool isSatisfied() const;

        const unsigned getSatClsCnt() const;

        const unsigned getClsCnt() const;

        unsigned int evaluate();

        ulong changeState();

        void returnChange(ulong t_i);

        void randomInit();

    private:
        unsigned m_max_val;
        unsigned m_max_weight;
        unsigned m_variable_cnt;
        unsigned m_cls_cnt;
        unsigned m_sat_cls_cnt;
        std::vector<bool> m_variables;
        std::vector<unsigned> m_weights;
        std::vector<Clause> m_clauses;
    };
}

#endif //MAXSAT_FORMULA_HPP
