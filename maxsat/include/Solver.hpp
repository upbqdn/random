//
// Created by marek on 1/19/18.
//

#ifndef MAXSAT_SOLVER_HPP
#define MAXSAT_SOLVER_HPP

#include "Formula.hpp"

namespace maxsat {
    class Solver {
    public:
        Solver(double t_alpha, int t_max_k, unsigned long long int t_init_T,
               bool t_print_graph, unsigned t_reps);

        unsigned solve(Formula &t_formula);

        const bool isSatisfied() const;

        std::chrono::duration<double> getDuration() const;

        const unsigned getSatClsCnt() const;

    private:
        double m_alpha;
        int m_max_k;
        unsigned long long m_init_T;
        unsigned m_best_sol;
        unsigned m_sat_cls_cnt;
        unsigned m_reps;
        bool m_plot_graph;
        std::chrono::duration<double> m_duration;
        bool m_satisfied;
    };
}

#endif //MAXSAT_SOLVER_HPP