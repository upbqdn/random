//
// Created by marek on 1/10/18.
//

#ifndef MAXSAT_CLAUSE_HPP
#define MAXSAT_CLAUSE_HPP

#include <vector>
#include <string>

namespace maxsat {
    class Clause {
    public:
        Clause(std::string t_description);

        const bool isSatisfied(const std::vector<bool> &t_variables) const;

    private:
        std::vector<int> m_literals;
    };
}


#endif //MAXSAT_CLAUSE_HPP
