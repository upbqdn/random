//
// Created by marek on 1/10/18.
//

#ifndef MAXSAT_CONSTANTS_HPP
#define MAXSAT_CONSTANTS_HPP

#include <string>
#include <vector>

namespace maxsat {
    static const std::string INST_PATH{
            "/home/marek/projects/maxsat/instances/"};
    static const std::string GRAPHS_PATH {
            "/home/marek/projects/maxsat/workspace/graphs/"};
    static const std::string NO_FILE{"No such file."};
    static const unsigned LITERAL_COUNT{3};

    enum Params {
        ALPHA = 1,
        K,
        INIT_T,
        DIRECTORY,
        GRAPHS,
        REPS
    };
}

#endif //MAXSAT_CONSTANTS_HPP
